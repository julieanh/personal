//Varibles
let gollum, gButton, mapRings, hobbits;
let volHobbits = false;

//Picture and sound is pre-loaded in order to avoid delay
function preload() {
  gollum = loadImage('gollum.png');
  mapRings = loadImage('mapRings.jpg');
  hobbits = loadSound("hobbits.mp3");

}

function setup() {
  createCanvas(500, 550);
//Background is set to be preloaded image set as a varible
  background(mapRings);

//Makes the audio loop but volume is set to 0 in order to control it further down
  hobbits.loop();
  hobbits.setVolume(0);

//This creates the custom button (here called gButton), here a picture of Gollum
  gButton = createImg('gollum.png');
  //This positions the button/image on the canvas
  gButton.position(80, 80);
//This I need to understand
  gButton.style("100px");
//This explains an action, that when mouse is clicked refer to the custom function
  gButton.mouseClicked(playHobbits);
}

//Custom function set here, used for mouseClicked
function playHobbits(){
  //Explains that if
  if (volHobbits){
    volHobbits = false;
    //Following tells us in console whether or not audio is playing
    console.log("Hobbits playing? ="+volHobbits);
    hobbits.setVolume(0);
  } else {
    volHobbits = true;
    console.log("Hobbits playing? ="+volHobbits);
    hobbits.setVolume(1);

  }

}
